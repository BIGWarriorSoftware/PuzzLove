import 'package:flutter/material.dart';
import 'game.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'PuzzLove';

    return MaterialApp(
      title: title,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: GridView.count(
          crossAxisCount: 2,
          children: List.generate(15, (index) {
            return Builder(
              builder: (context) => RaisedButton(
                  padding: EdgeInsets.all(5.0),
                  hoverColor: Color.fromARGB(1, 0, 0, 255),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new Game(index)));
                  },
                  child: Image(
                    image: AssetImage(
                        'assets/images/img' + index.toString() + '.jpg'),
                    width: 100,
                  )),
            );
          }),
        ),
      ),
    );
  }
}
